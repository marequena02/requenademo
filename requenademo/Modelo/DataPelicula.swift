//
//  Resumen.swift
//  TannerFactoring
//

import Foundation


open class DataPelicula: JSONEncodable {
    

    public var title: String?
    
    public var poster_path: String?
    
    public var original_language: String?
    
    public var overview: String?
    
    public var release_date: String?
    
    
    
    public init() {}
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> Any {
        var nillableDictionary = [String:Any?]()

        nillableDictionary["title"] = self.title
        nillableDictionary["poster_path"] = self.poster_path
        nillableDictionary["original_language"] = self.original_language
        nillableDictionary["overview"] = self.overview
        nillableDictionary["release_date"] = self.release_date

        
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}

