//
//  ViewController.swift
//  requenademo
//

import UIKit

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggenIn")
        
        if(!isUserLoggedIn){
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "Login")
            self.present(loginVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "isUserLoggenIn");
        UserDefaults.standard.set(false, forKey: "beaconsBD");
        UserDefaults.standard.synchronize();
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "Login")
        self.present(loginVC, animated: true, completion: nil)
        

        
    }
    
}

