//
//  DocTableViewCell.swift
//  Tanner
//
//  Created by Miguel Requena on 12/15/17.
//  Copyright © 2017 Cocoacasts. All rights reserved.
//

import UIKit

class DocTopTableViewCell: UITableViewCell {


    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var fecPublic: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var notes: UILabel!
    
    @IBAction func SelectCell(_ sender: UISwitch) {
        
    }
    

}
