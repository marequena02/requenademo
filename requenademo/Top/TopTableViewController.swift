//
//  DocsTableViewController.swift
//  Tanner
//
//  Created by Miguel Requena on 12/15/17.
//  Copyright © 2017 Cocoacasts. All rights reserved.
//

import UIKit
import Alamofire

class TopTableViewController:  BaseViewController, UITableViewDataSource, UITableViewDelegate {

    
    var ListDocs = [DataPelicula]()
 
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        self.title = "Populares"
        
 
    
        ObtenerPelicula()
        
        
    }

    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        DispatchQueue.main.async {
            //self.reloadData = false
            self.ListDocs.removeAll()
            self.ObtenerPelicula()
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
        
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
    
        return self.ListDocs.count
    }
    
   
    
//    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        self.docCurrent = self.ListDocs[indexPath.row]
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.layer.shadowOffset = CGSize(width: 0,height: 5);
        cell.layer.shadowRadius = 5;
        cell.layer.shadowOpacity = 0.5;
        cell.layer.masksToBounds = false;
    }
    
    

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellDocMovie", for: indexPath) as? DocTopTableViewCell  else {
            fatalError("The dequeued cell is not an instance of Cartera.")
        }
        cell.title.text = self.ListDocs[indexPath.row].title
        cell.fecPublic.text = self.ListDocs[indexPath.row].release_date
        cell.notes.text = self.ListDocs[indexPath.row].overview
        let imageUrlString = "http://image.tmdb.org/t/p/w500"+ListDocs[indexPath.row].poster_path!
        let imageUrl:URL = URL(string: imageUrlString)!
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
           
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                cell.imgView.image = image
                cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            }
        }
        
        return cell
    }

    
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let detViewController = segue.destination as! DetViewController
        detViewController.DataM = self.ListDocs[(tableView.indexPathForSelectedRow?.row)!]
    }
    
    
    
    
    func ObtenerPelicula(){
        let urlstring = "https://api.themoviedb.org/3/movie/top_rated?api_key=34738023d27013e6d1b995443764da44"
        
        let postString: [String: Any] = [
            "":""
        ]
        Alamofire.request(urlstring, method: .get, parameters: postString, encoding: JSONEncoding.default)
            .responseJSON { response in
                let json = response.result.value as? NSDictionary
                let movies = json!["results"] as? NSArray
                self.ListDocs = Decoders.decode(clazz: [DataPelicula].self, source: movies as AnyObject)
                self.tableView.reloadData()
                self.do_table_refresh()
               
        }
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.do_table_refresh()
    }
    
    func do_table_refresh()
    {
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
            return
        })
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        tableView.reloadData()
        
    }

}
