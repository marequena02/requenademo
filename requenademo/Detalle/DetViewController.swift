//
//  DetViewController.swift
//  requenademo
//


import UIKit

class DetViewController: UIViewController {

    public var DataM = DataPelicula()
    
   
    @IBOutlet weak var titleT: UILabel!
    
    @IBOutlet weak var fechaP: UILabel!
    
    @IBOutlet weak var imageV: UIImageView!
    
    @IBOutlet weak var notes: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleT.text = self.DataM.title
        self.fechaP.text = self.DataM.release_date
        self.notes.text = self.DataM.overview
        let imageUrlString = "http://image.tmdb.org/t/p/w500"+DataM.poster_path!
        let imageUrl:URL = URL(string: imageUrlString)!
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            
            
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                self.imageV.image = image
                self.imageV?.contentMode = UIViewContentMode.scaleAspectFit
                
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
