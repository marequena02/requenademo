//
//  BaseViewController.swift
//  Tanner
//
//  Created by Miguel Requena on 12/14/17.
//  Copyright © 2017 Cocoacasts. All rights reserved.
//

import UIKit
import Alamofire

class BaseViewController: UIViewController {

    public var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    public static let VigenteData = "Populares"
    public static let VigentePag = "Top"
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
            //mScrollView.setContentOffset(CGPoint(x: coordenada, y:0), animated: true)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    public func displayMyAlertMessage (userMessage:String)
    {
        let myAlert = UIAlertController(title: "Tanner", message: userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil);
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated:true, completion:nil);
    }
    



    
    
}



